CREATE TABLE adherent
(
  id integer NOT NULL,
  prenom text,
  nom text
);

create temporary table adherent_tmp
(id int, nom text, prenom text, d text, e text, f text);

copy adherent_tmp (id, nom, prenom, d, e, f)
  from '/docker-entrypoint-initdb.d/adherent.csv'
  delimiter ','
  csv header quote '"' escape ''''
  encoding 'utf8';

insert into adherent (id, nom, prenom)
select id, nom, prenom from adherent_tmp;

drop table adherent_tmp;

-- fin de l'import des données

alter table adherent add primary key (id);

CREATE INDEX idx_adherent_nom ON adherent USING btree (nom ASC NULLS LAST);
