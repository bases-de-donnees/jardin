drop table if exists `adherent`;

create table if not exists `adherent` (
  `id` int(11) not null,
  `intitule` varchar(255) not null,
  `adresse` varchar(255) DEFAULT NULL,
  `cp` varchar(255) DEFAULT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `localisation` point DEFAULT NULL,
  `depot` int(11) DEFAULT NULL
);

load data infile '/docker-entrypoint-initdb.d/adherent.csv'
replace into table adherent
fields terminated by ','
lines terminated by '\n'
ignore 1 lines
(id, intitule);

alter table `adherent`
  add primary key (`id`);

alter table `adherent`
  modify `id` int(11) not null auto_increment;
