drop table if exists `frequence`;

create table if not exists `frequence` (
  `id` int(11) not null,
  `intitule` varchar(255) not null
);

load data infile '/docker-entrypoint-initdb.d/frequence.csv'
replace into table frequence
fields terminated by ','
lines terminated by '\n'
ignore 1 lines
(id, intitule);

alter table `frequence`
  add primary key (`id`);

alter table `frequence`
  modify `id` int(11) not null auto_increment;
