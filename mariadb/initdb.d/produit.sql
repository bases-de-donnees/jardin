

CREATE TABLE `produit` (
  `id` int(11) NOT NULL,
  `produit` varchar(255) NOT NULL
);

ALTER TABLE `produit`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `tournee` (
  `id` int(11) NOT NULL,
  `tournee` varchar(255) NOT NULL
);

ALTER TABLE `tournee`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tournee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
