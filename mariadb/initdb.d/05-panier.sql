drop table if exists `panier`;

create table if not exists `panier` (
  `id` int(11) not null,
  `intitule` varchar(255) not null
);

load data infile '/docker-entrypoint-initdb.d/panier.csv'
replace into table panier
fields terminated by ','
lines terminated by '\n'
ignore 1 lines
(id, intitule);

alter table `panier`
  add primary key (`id`);

alter table `panier`
  modify `id` int(11) not null auto_increment;
