drop table if exists `depot`;

create table if not exists `depot` (
  `id` int(11) not null,
  `depot` varchar(255) not null,
  `adresse` varchar(255),
  `codepostal` varchar(255),
  `ville` varchar(255),
  `localisation` point
);

load data infile '/docker-entrypoint-initdb.d/depot.csv'
replace into table depot
fields terminated by ','
lines terminated by '\n'
ignore 1 lines
(id, depot, adresse, codepostal, ville, @localisation)
set localisation = ST_GeomFromText(@localisation);

alter table `depot`
  add primary key (`id`);

alter table `depot`
  modify `id` int(11) not null auto_increment;
