drop table if exists `abonnement`;

create table if not exists `abonnement` (
  `id` int(11) not null,
  `id_adherent` int(11) not null,
  `id_panier` int(11) not null,
  `id_frequence` int(11) not null,
  `debut` date,
  `fin` date
);

-- 67 % des adhérents ont un panier, les autres sont des soutiens
-- 60 % des livraisons sont hebdomadaires, 25% sont quizomadaires, 15% sont mensuelles

DELIMITER //

FOR i IN 1..2800
DO
  IF (RAND() < 0.67) THEN

    SET @p = FLOOR(1 + RAND() * (6));
    SET @f = RAND();

    insert into `abonnement` (id_adherent, id_panier, id_frequence) values (
      i,
      FLOOR(1 + RAND() * (5)),
      CASE
    	WHEN @f < 0.6 THEN 1
    	WHEN @f < 0.85 THEN 2
    	ELSE 3
      END
    );
  END IF;
END FOR;
//

DELIMITER ;
